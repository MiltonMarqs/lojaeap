package pxt.loja.domain.cadastro;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "MILTON.tcidade")
public class Cidade implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "codcid")
	@GeneratedValue(generator = "autoinc")
	@GenericGenerator(name = "autoinc", strategy = "pxt.framework.persistence.hibernate.NextNumberIdentifierGenerator")
	private Integer codcid;
	@Column(name = "nomcid")
	private String nomeCidade;

	@ManyToOne
	@JoinColumn(name = "codest", referencedColumnName = "codest")
	private Estado estado;

	public Integer getCodcid() {
		return codcid;
	}

	public void setCodcid(Integer codcid) {
		this.codcid = codcid;
	}

	public String getNomeCidade() {
		return nomeCidade;
	}

	public void setNomeCidade(String nomeCidade) {
		this.nomeCidade = nomeCidade;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return nomeCidade;
	}
	
	
}
