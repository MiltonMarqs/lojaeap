package pxt.loja.domain.cadastro;

import java.io.Serializable;

public class Cliente implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Pessoa pessoa;
	private String nome;
	private String endereco;
	private long telefone;
	private int tamanhoNome;

	

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public long getTelefone() {
		return telefone;
	}

	public void setTelefone(long telefone) {
		this.telefone = telefone;
	}

	public int getTamanhoNome() {
		return tamanhoNome;
	}


	public Cliente(String nome, String endereco, long telefone) {
		
		this.nome = nome;
		this.endereco = endereco;
		this.telefone = telefone;
	}

	public Cliente() {
	}
}
