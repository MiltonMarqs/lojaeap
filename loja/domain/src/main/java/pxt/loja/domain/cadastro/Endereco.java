package pxt.loja.domain.cadastro;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "MILTON.tendereco")
public class Endereco implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "codend")
	@GeneratedValue(generator = "autoinc")
	@GenericGenerator(name = "autoinc", strategy = "pxt.framework.persistence.hibernate.NextNumberIdentifierGenerator")
	private Integer codend;

	@Column(name = "NOMLOG")
	private String logradouro;
	@Column(name = "NOMBAI")
	private String bairro;
	@Column(name = "NUMRSD")
	private String numeroCasa;
	@Column(name = "TIPLOG")
	private String tipoLog;
	@Column(name = "NUMCEP")
	private String cep;
	
	@OneToOne
	@JoinColumn(name = "codpes", referencedColumnName = "codpes")
	 private Pessoa pessoa;
	
	
	@OneToOne(targetEntity = Cidade.class)
	@JoinColumn(name = "codcid", referencedColumnName = "codcid")
	private Cidade cidade;

	

	public Integer getCodend() {
		return codend;
	}

	public void setCodend(Integer codend) {
		this.codend = codend;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getNumeroCasa() {
		return numeroCasa;
	}

	public void setNumeroCasa(String numeroCasa) {
		this.numeroCasa = numeroCasa;
	}

	public String getTipoLog() {
		return tipoLog;
	}

	public void setTipoLog(String tipoLog) {
		this.tipoLog = tipoLog;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	
	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	
}

