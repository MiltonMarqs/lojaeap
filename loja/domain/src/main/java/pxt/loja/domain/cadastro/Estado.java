package pxt.loja.domain.cadastro;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "MILTON.testado")
public class Estado implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "autoinc")
	@GenericGenerator(name = "autoinc", strategy = "pxt.framework.persistence.hibernate.NextNumberIdentifierGenerator")
	@Column(name = "codest")
	private Integer codigoEstado;
	@Column(name = "nomest")
	private String nomeEstado;
	@Column(name = "sigest")
	private String siglaEstado;

	@OneToMany
	@JoinColumn(name = "codest", referencedColumnName = "codest")
	private List<Cidade> cidade;

	public List<Cidade> getCidade() {
		return cidade;
	}

	public void setCidade(List<Cidade> cidade) {
		this.cidade = cidade;
	}

	public Integer getCodigoEstado() {

		return codigoEstado;
	}

	public void setCodigoEstado(Integer codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	public String getNomeEstado() {
		return nomeEstado;
	}

	public void setNomeEstado(String nomeEstado) {
		this.nomeEstado = nomeEstado;
	}

	public String getSiglaEstado() {
		return siglaEstado;
	}

	public void setSiglaEstado(String siglaEstado) {
		this.siglaEstado = siglaEstado;
	}

	@Override
	public String toString() {
		return nomeEstado;
	}

}
