package pxt.loja.domain.cadastro;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;





import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "MILTON.tpessoa")
public class Pessoa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "codpes")
	@GeneratedValue(generator = "autoinc")
	@GenericGenerator(name = "autoinc", strategy = "pxt.framework.persistence.hibernate.NextNumberIdentifierGenerator")
	private Integer codigo;

	@Column(name = "cpfcnpj")
	private String cpfcnpj;
	@Column(name = "nompes")
	private String nome;
	
	@OneToOne(targetEntity = Endereco.class)
	@JoinColumn(name = "codpes", referencedColumnName = "codpes")
	 private Endereco endereco;
	
	@Column(name = "datnas")
	private Date datanasc; 
	
	@Enumerated (EnumType.STRING)
	@Column (name = "tippes")
	private TipoPessoa tipopessoa;
	
	public String getCpfcnpj() {
		return cpfcnpj;
	}

	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

//	public List<Endereco> getEndereco() {
//		return endereco;
//	}
//
//	public void setEndereco(List<Endereco> endereco) {
//		this.endereco = endereco;
//	}

	 public Endereco getEndereco() {
	 return endereco;
	 }
	
	 public void setEndereco(Endereco endereco) {
	 this.endereco = endereco;
	 }

	public Date getDatanasc() {
		return datanasc;
	}

	public void setDatanasc(Date datanasc) {
		this.datanasc = datanasc;
	}

	public TipoPessoa getTipopessoa() {
		return tipopessoa;
	}

	public void setTipopessoa(TipoPessoa tipopessoa) {
		this.tipopessoa = tipopessoa;
	}
 
	
	
	
	

}
