package pxt.loja.domain.estoque;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "MILTON.tproduto")
public class Produto implements Serializable {

	private static final long serialVersionUID = 1L;
   	
	@Id
	@GeneratedValue(generator = "autoinc")
	@GenericGenerator(name = "autoinc", strategy = "pxt.framework.persistence.hibernate.NextNumberIdentifierGenerator")
	@Column(name = "codpro")
	private Integer codigo;
	@Column(name = "nompro")
    private String nome;
	@Column(name = "despro")
    private String descricao;
   // private String imagem;
    @Column(name = "vlrpro")
    private Double valor;
    @Column(name = "qdeetq")
    private Integer estoque;


    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

//    public String getImagem() {
//        return imagem;
//    }
//
//    public void setImagem(String imagem) {
//        this.imagem = imagem;
//    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getEstoque() {
        return estoque;
    }

    public void setEstoque(Integer estoque) {

        this.estoque = estoque;
    }


    public Produto(Integer codigo, String nome, String descricao, Double valor, Integer estoque) {
        this.codigo = codigo;
        this.nome = nome;
        this.descricao = descricao;
        this.valor = valor;
        this.estoque = estoque;
    }

    public Produto() {
    }

    @Override
    public String toString() {
        return  " Produto " +
                " Codigo : " + codigo + "\n" +
                " Item : " + nome + "\n" +
                " Descricao : " + descricao + "\n" +
                " valor  PRODUTO: R$ " + valor + "\n" +
                " Estoque : " + estoque;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Produto)) return false;
        Produto produto = (Produto) o;
        return Objects.equals(codigo, produto.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }
    }
