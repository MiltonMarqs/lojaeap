package pxt.loja.domain.pedido;

import java.io.Serializable;
import java.util.Objects;

import pxt.loja.domain.estoque.Produto;

public class ItemPedido implements Serializable {
    
	private static final long serialVersionUID = 1L;
	
	private Produto produto;
    private Integer quantidade;
    private Double preco;

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }


    public ItemPedido() {

    }

    @Override
    public String toString() {
        return "Itens do Pedido : "
                + "produto : " + produto + "\n"
                + " quantidade : " + quantidade + "\n"
                + " preco : R$ " + preco;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ItemPedido)) return false;
        ItemPedido that = (ItemPedido) o;
        return Objects.equals(produto, that.produto) && Objects.equals(quantidade, that.quantidade);
    }

    @Override
    public int hashCode() {
        return Objects.hash(produto, quantidade);
    }
}
