package pxt.loja.domain.pedido;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pxt.loja.domain.cadastro.Cliente;

public class Pedido implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer codigo;
	private List<ItemPedido> listaItemPedido;
	private Date dataPedido;
	private Cliente cliente;

	public Pedido() {
		listaItemPedido = new ArrayList<ItemPedido>();
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public List<ItemPedido> getListaItemPedido() {
		return listaItemPedido;
	}


	public Date getDataPedido() {
		return dataPedido;
	}

	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}

	public Cliente getCliente() {

		return cliente;
	}

	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}


	public void adicionaItemPedido(ItemPedido itemPedido) {
		listaItemPedido.add(itemPedido);
	}

	public ItemPedido getItemPorCodigoProduto(Integer codigo) {
		ItemPedido itemEncontrado = null;
		for(ItemPedido itemNaLista : getListaItemPedido()) {
			if ( itemNaLista.getProduto().getCodigo().equals(codigo)) {
				itemEncontrado = itemNaLista;
			}
		}
		return itemEncontrado;
	}

	@Override
	public String toString() {
		return 	"Pedido :" + codigo + "\n" +
				listaItemPedido + "\n" +
				" Data Pedido :" + dataPedido + "\n" +
				" cliente : " + this.cliente + "\n";

	}
}