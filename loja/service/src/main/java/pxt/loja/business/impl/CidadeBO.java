package pxt.loja.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pxt.loja.domain.cadastro.Cidade;
import pxt.loja.persistence.impl.CidadeDAOImpl;

@Stateless
public class CidadeBO {

	@EJB
	private CidadeDAOImpl cidadeDAOImpl;
	
	public List<Cidade> buscartodos(){
		
		return cidadeDAOImpl.buscarTodos();
		
	}
}
