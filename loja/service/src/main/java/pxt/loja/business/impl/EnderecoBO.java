package pxt.loja.business.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pxt.framework.persistence.PersistenceException;
import pxt.loja.domain.cadastro.Endereco;
import pxt.loja.persistence.impl.EnderecoDAOImpl;

@Stateless
public class EnderecoBO  {

	@EJB
	private EnderecoDAOImpl enderecoDAOImpl;
	
	public Endereco salvar(Endereco endereco) throws PersistenceException{
		return enderecoDAOImpl.saveOrUpdate(endereco);
	}
}
