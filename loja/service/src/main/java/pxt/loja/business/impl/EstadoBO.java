package pxt.loja.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pxt.loja.domain.cadastro.Estado;
import pxt.loja.persistence.impl.EstadoDAOImpl;

@Stateless
public class EstadoBO {

	@EJB
	private EstadoDAOImpl estadoDAOImpl;
	
	public List<Estado> buscartodos(){
		
		return estadoDAOImpl.buscarTodos();
		
	}

}
