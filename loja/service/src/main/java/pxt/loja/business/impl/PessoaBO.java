package pxt.loja.business.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import pxt.framework.persistence.PersistenceException;
import pxt.loja.domain.cadastro.Endereco;
import pxt.loja.domain.cadastro.Pessoa;
import pxt.loja.domain.cadastro.TipoPessoa;
import pxt.loja.persistence.impl.PessoaDAOImpl;

@Stateless
public class PessoaBO {

	@EJB
	private PessoaDAOImpl pessoaDAOImpl;

	@EJB
	private EnderecoBO enderecoBO;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Pessoa salvar(Pessoa pessoa) throws PersistenceException {
		Endereco endereco = pessoa.getEndereco();
		tipoPessoa(pessoa);
		pessoa = pessoaDAOImpl.saveOrUpdate(pessoa);
		endereco.setPessoa(pessoa);
		enderecoBO.salvar(endereco);
		pessoa.setEndereco(endereco);
		return pessoa;
	}

	public void tipoPessoa(Pessoa pessoa) {

		if (pessoa.getCpfcnpj().length() == 14) {
			pessoa.setTipopessoa(TipoPessoa.Juridica);
		} else if (pessoa.getCpfcnpj().length() == 11) {
			pessoa.setTipopessoa(TipoPessoa.Fisica);
		} else {
			throw new RuntimeException("CPF ou CNPJ incorreto");
		}
	}

	// public static TipoPessoa tipoPessoaPorCpfCnpj(String cpfCnpj) {
	// if(cpfCnpj.length() == TipoPessoa.FISICA.getTamanho()){
	// return TipoPessoa.FISICA;
	// } else if (cpfCnpj.length() == TipoPessoa.JURIDICA.getTamanho()){
	// return TipoPessoa.JURIDICA;
	// }
	// throw new RuntimeException("CPF CNPJ informado com tamanho errado");
	// }
	//

}
