package pxt.loja.persistence.impl;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;

import pxt.loja.domain.cadastro.Cidade;

@Stateless
public class CidadeDAOImpl extends LojaHibernateDAO<Cidade, Integer> {

	 public List buscarTodos() {
			
		 Criteria criteria = getSession().createCriteria(Cidade.class);
		
		 return criteria.list();
		
		 }
	
	
}
