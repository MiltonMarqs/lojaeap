package pxt.loja.persistence.impl;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;

import pxt.loja.domain.cadastro.Estado;

@Stateless
public class EstadoDAOImpl extends LojaHibernateDAO<Estado, Integer> {

	 public List buscarTodos() {
	
	 Criteria criteria = getSession().createCriteria(Estado.class);
	
	 return criteria.list();
	
	 }
	

}
