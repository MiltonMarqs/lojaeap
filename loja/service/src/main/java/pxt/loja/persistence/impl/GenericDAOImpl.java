package pxt.loja.persistence.impl;

import javax.ejb.Local;
import javax.ejb.Stateless;

import pxt.framework.persistence.DataAccessObject;

@Stateless(name = "loja." + DataAccessObject.SERVICE_NAME)
@Local(DataAccessObject.class)
public class GenericDAOImpl extends LojaHibernateDAO {}
