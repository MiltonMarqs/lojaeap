package pxt.loja.persistence.impl;

import java.io.Serializable;

import javax.persistence.PersistenceContext;

import org.hibernate.Session;

import pxt.framework.persistence.hibernate.HibernateIDAO;

public class LojaHibernateDAO<T, ID extends Serializable> extends HibernateIDAO<T, T, ID> {

	@PersistenceContext(unitName = "LOJADS")
	private Session session;

	public LojaHibernateDAO() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pxt.framework.persistence.hibernate.HibernateIDAO#getSession()
	 */
	@Override
	protected Session getSession() {
		return session;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pxt.framework.persistence.hibernate.HibernateIDAO#setSession(org.hibernate
	 * .Session)
	 */
	@Override
	protected void setSession(Session session) {
		this.session = session;
	}
}
