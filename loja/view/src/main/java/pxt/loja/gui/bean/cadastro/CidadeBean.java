package pxt.loja.gui.bean.cadastro;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import pxt.framework.business.PersistenceService;
import pxt.framework.faces.controller.CrudController;
import pxt.loja.domain.cadastro.Cidade;

@ManagedBean
@ViewScoped
public class CidadeBean extends CrudController<Cidade> {

	private static final long serialVersionUID = 1L;
	@EJB
	private PersistenceService persistenceService;
	private Cidade domain;

	@Override
	public Cidade getDomain() {
		if (domain == null) {
			domain = new Cidade();
		}

		return domain;
	}

	@Override
	public void setDomain(Cidade domain) {
		this.domain = domain;
	}

	@Override
	public PersistenceService getPersistenceService() {
		return persistenceService;
	}

}
