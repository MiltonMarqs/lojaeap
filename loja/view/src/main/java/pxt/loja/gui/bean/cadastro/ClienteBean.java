package pxt.loja.gui.bean.cadastro;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import pxt.framework.business.PersistenceService;
import pxt.framework.business.TransactionException;
import pxt.framework.faces.controller.CrudController;
import pxt.framework.faces.exception.CrudException;
import pxt.loja.domain.cadastro.Cliente;
import pxt.loja.domain.estoque.Produto;


@ManagedBean
@ViewScoped
public class ClienteBean extends CrudController<Cliente>{

	private static final long serialVersionUID = 1L;
	
	private PersistenceService persistenceService;
	private Cliente domain;
	
	@Override
	protected void salvar() throws CrudException, TransactionException {
		System.out.println(getDomain().getNome());
		System.out.println(getDomain().getEndereco());
		System.out.println(getDomain().getTelefone());
	}

	@Override
	public Cliente getDomain() {
		if(domain == null){
			domain = new Cliente();
		}
		return domain;
	}

	@Override
	public void setDomain(Cliente domain) {
		this.domain = domain;		
	}

	@Override
	public PersistenceService getPersistenceService() {
		return persistenceService;
	}

}
