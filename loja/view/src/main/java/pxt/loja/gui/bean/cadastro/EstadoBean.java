package pxt.loja.gui.bean.cadastro;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import pxt.framework.business.PersistenceService;
import pxt.framework.faces.controller.CrudController;
import pxt.loja.domain.cadastro.Estado;
import pxt.loja.persistence.impl.EstadoDAOImpl;

@ManagedBean
@ViewScoped
public class EstadoBean extends CrudController<Estado> {

	private static final long serialVersionUID = 1L;

	@EJB
	private PersistenceService persistenceService;
	private Estado domain;
	private List<Estado> estados;
	@EJB
	private EstadoDAOImpl estadoDAOimpl;

	@Override
	public Estado getDomain() {
		if (domain == null) {
			domain = new Estado();
		}
		return domain;
	}

	@Override
	public void setDomain(Estado domain) {
		this.domain = domain;
	}

	@Override
	public PersistenceService getPersistenceService() {
		return persistenceService;
	}

	public List<Estado> getEstados() {
		if (estados == null) {
			estados = estadoDAOimpl.buscarTodos();

		}
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

}
