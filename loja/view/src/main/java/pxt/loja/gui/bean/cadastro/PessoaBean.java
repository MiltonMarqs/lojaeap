package pxt.loja.gui.bean.cadastro;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import pxt.framework.business.PersistenceService;
import pxt.framework.business.TransactionException;
import pxt.framework.faces.controller.CrudController;
import pxt.framework.faces.exception.CrudException;
import pxt.framework.persistence.PersistenceException;
import pxt.loja.business.impl.CidadeBO;
import pxt.loja.business.impl.EstadoBO;
import pxt.loja.business.impl.PessoaBO;
import pxt.loja.domain.cadastro.Cidade;
import pxt.loja.domain.cadastro.Endereco;
import pxt.loja.domain.cadastro.Estado;
import pxt.loja.domain.cadastro.Pessoa;

@ManagedBean
@ViewScoped
public class PessoaBean extends CrudController<Pessoa> {

	private static final long serialVersionUID = 1L;

	@EJB
	private PersistenceService persistenceService;
	private Pessoa domain;
	private List<Estado> listaEstado;
	private List<Cidade> listaCidade;
	
	@EJB
	private EstadoBO estadoBO;
	@EJB
	private CidadeBO cidadeBO;
	@EJB
	private PessoaBO pessoaBO;


	private Estado estado;
	private Cidade cidade;
	
	private Endereco endereco;

	@Override
	public Pessoa getDomain() {
		if (domain == null) {
			domain = new Pessoa();
		}
		return domain;
	}

	@Override
	public void setDomain(Pessoa domain) {
		this.domain = domain;

	}

	public List<Estado> getListaEstado() {
		return listaEstado;
	}

	public void setListaEstado(List<Estado> listaEstado) {
		this.listaEstado = listaEstado;
	}

	public EstadoBO getEstadoBO() {
		return estadoBO;
	}

	public void setEstadoBO(EstadoBO estadoBO) {
		this.estadoBO = estadoBO;
	}
	
	

	public CidadeBO getCidadeBO() {
		return cidadeBO;
	}

	public void setCidadeBO(CidadeBO cidadeBO) {
		this.cidadeBO = cidadeBO;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	



	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public List<Cidade> getListaCidade() {
		return listaCidade;
	}

	public void setListaCidade(List<Cidade> listaCidade) {
		this.listaCidade = listaCidade;
	}

	public Endereco getEndereco() {
		if (endereco == null) {
			endereco = new Endereco();
		}
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public PersistenceService getPersistenceService() {
		return persistenceService;
	}

	@Override
	protected void antesSalvar() throws CrudException {
		if (this.domain.getCpfcnpj() == null
				|| this.domain.getCpfcnpj().isEmpty()) {
			msgWarn("CPF n�o pode ser nulo");
			throw new CrudException();

		} else if (this.domain.getNome() == null
				|| this.domain.getNome().isEmpty()) {
			msgWarn("Nome n�o pode ser nulo");
			throw new CrudException();

		}

		super.antesSalvar();
	}

	@Override
	protected void novo() {
		setListaEstado(estadoBO.buscartodos());
		setListaCidade(cidadeBO.buscartodos());
	}
	
	@Override
	protected void salvar() throws CrudException, TransactionException {
		getEndereco().setCidade(getCidade());
		getDomain().setEndereco(getEndereco());
		try {
			pessoaBO.salvar(getDomain());
		} catch (PersistenceException e) {
			e.printStackTrace();
			throw new CrudException();
		}
	}
}
