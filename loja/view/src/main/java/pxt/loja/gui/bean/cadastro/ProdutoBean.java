package pxt.loja.gui.bean.cadastro;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import pxt.framework.business.PersistenceService;
import pxt.framework.faces.controller.CrudController;
import pxt.framework.faces.exception.CrudException;
import pxt.loja.domain.estoque.Produto;

@ManagedBean
@ViewScoped
public class ProdutoBean extends CrudController<Produto> {

	private static final long serialVersionUID = 1L;
	@EJB
	private PersistenceService persistenceService;
	private Produto domain;

	@Override
	public Produto getDomain() {
		if (domain == null) {
			domain = new Produto();
		}
		return domain;
	}

	@Override
	public void setDomain(Produto domain) {
		this.domain = domain;
	}

	@Override
	public PersistenceService getPersistenceService() {
		return persistenceService;
	}

	@Override
	public void antesSalvar() throws CrudException {

		if (this.domain.getNome() == null || this.domain.getNome().isEmpty()) {

			msgWarn("Nome do produto � obrigat�rio");
			throw new CrudException();

		} else if (this.domain.getValor() == null
				|| this.domain.getValor() == 0) {
			msgWarn("Valor do produto nao pode ser zero");
			throw new CrudException();

		} else if (this.domain.getEstoque() == null) {
			msgWarn("Quantidade do produto nao pode ser nula");
			throw new CrudException();

		}

		super.antesSalvar();
	}

}
